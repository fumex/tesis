import { DetailListComponent } from './../../label/components/detail-list.component';
import { LabelViewComponent } from './../../label/components/label-view.component';
import { LabelEditComponent } from './../../label/components/label-edit.component';
import { NgModule } from '@angular/core';
import {RouterModule, Router, ActivatedRoute, Routes} from '@angular/router';
import { AdminComponent } from '../admin.component';
import { AdminContentComponent } from '../admin-content/admin-content.component';
import { LabelAddComponent } from '../../label/components/label-add.component';
import { LabelListComponent } from '../../label/components/label-list.component';
import { DetailAddComponent } from '../../label/components/detail-add.component';
import { PredictionComponent } from '../../prediction/components/prediction.component';

@NgModule({
  imports:[
    RouterModule.forChild([
      {
        path: 'admin',
        component: AdminComponent,
        children:[
          {
            path: '',
            component: AdminContentComponent
          },
          {
            path: 'label',
            component: LabelAddComponent
          },
          {
            path: 'label/list',
            component: LabelListComponent
          },
          {
            path: 'label/edit/:id',
            component: LabelEditComponent
          },
          {
            path: 'label/views/:id',
            component: LabelViewComponent
          },
          {
            path: 'detail',
            component: DetailAddComponent
          },
          {
            path: 'detail/list',
            component: DetailListComponent
          },
          {
            path: 'prediction',
            component: PredictionComponent
          }
        ]
      }
    ]),
  ],
  exports:[
    RouterModule
  ]
})

export  class AdminRoutingModule{}
