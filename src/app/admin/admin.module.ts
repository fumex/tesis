import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { CommonModule } from '@angular/common';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminContentComponent } from './admin-content/admin-content.component';
import { AdminRoutingModule } from './admin-routing/admin-routing.module';


@NgModule({
  imports:[
    CommonModule,
    AdminRoutingModule
  ],
  declarations:[
    AdminComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    AdminContentComponent,

  ],
  exports:[AdminComponent]
})
export class AdminModule{}
