import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, Routes, ActivatedRoute } from '@angular/router';
import { AdminContentComponent } from '../admin/admin-content/admin-content.component';
@NgModule({
  imports: [
    RouterModule.forRoot([

      {
        path: '', redirectTo: 'admin' , pathMatch: 'full'
      }
    ])
  ],
  declarations:[ ],
  exports : [RouterModule]
})
export class AppRoutingModule{
}
