import { LabelDetailModel } from './../models/label_detail.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LabelModel } from '../models/label.model';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';



@Injectable()
export class LabelService{
  constructor(
    private http: HttpClient
  ){}

  addLabel(label: LabelModel): Observable<any>{
    let params = JSON.stringify(label);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(`${environment.api_url}/label`, params, {headers: headers});
  }

  getLabels():Observable<any[]>{

    return this.http.get<any>(`${environment.api_url}/lista_labels`);

  }

  getLabel(id: string):Observable<any>{
    return this.http.get<any>(`${environment.api_url}/label/` + id);
  }

  deleteLabels(id: string):Observable<any>{
    return this.http.get<any>(`${environment.api_url}/delete/` + id);
  }

  updateLabel(id: string, label:LabelModel):Observable<any>{
    let params = JSON.stringify(label);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(`${environment.api_url}/label/` + id, params, { headers: headers});
  }

  getLabelsDetail(id: string): Observable<any[]>{
    return this.http.get<any>(`${environment.api_url}/label/details/` + id);
  }

  addLabelDetail(detail: LabelDetailModel): Observable<any>{
    let params = JSON.stringify(detail);
    let headers =  new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(`${environment.api_url}/label/detail`, params, {headers: headers});
  }

  getDetails():Observable<any[]>{
    return this.http.get<any>(`${environment.api_url}/label/details`);
  }
  deleteDetail(id: string): Observable<any>{
    return this.http.get<any>(`${environment.api_url}/details/` + id);
  }

  updateDetail(id: string, detail:LabelDetailModel):Observable<any>{
    let params = JSON.stringify(detail);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(`${environment.api_url}/details/` + id, params, {headers: headers});
  }
}
