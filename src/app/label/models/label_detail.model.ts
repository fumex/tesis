export class LabelDetailModel {
  constructor(
      public treatment: string,
      public url: string,
      public id_label: string,
  ){}
}
