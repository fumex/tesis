export class LabelModel{
  constructor(
      public id:string,
      public name:string,
      public symbol:string,
      public description:string,
  ){}
}
