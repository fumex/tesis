import { DetailListComponent } from './components/detail-list.component';
import { DetailAddComponent } from './components/detail-add.component';
import { LabelViewComponent } from './components/label-view.component';
import { LabelListComponent } from './components/label-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';

import { LabelAddComponent } from './components/label-add.component';
import { LabelService } from './services/label.service';
import { LabelEditComponent } from './components/label-edit.component';
@NgModule({
  imports:[
    CommonModule,
    FormsModule,
  ],
  declarations:[
    LabelAddComponent,
    LabelEditComponent,
    LabelListComponent,
    LabelViewComponent,
    DetailAddComponent,
    DetailListComponent,
  ],
  providers:[
    LabelService
  ]
})
export class LabelModule{}
