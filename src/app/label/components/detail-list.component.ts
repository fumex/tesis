import { ActivatedRoute, Router } from '@angular/router';
import { LabelService } from '../services/label.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector:'detail-list',
  templateUrl:'../views/detail-list.html',
  providers:[LabelService]
})
export class DetailListComponent implements OnInit{
  public title:string;
  public details: any[];
  public labels: any[];

  constructor(
    private labelService: LabelService,
    private route: ActivatedRoute,
    private router: Router
  ){
    this.title='Lista Detalle de Nutrientes';
  }
  ngOnInit(){
    this.getDetails();
    this.getLabel();
  }

  getDetails(){
    this.labelService.getDetails().subscribe(
      response => {
        this.details = response['data'];
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  getLabel(){
    this.labelService.getLabels().subscribe(
      response =>{
        this.labels = response['data'];
        console.log(this.labels);
      },
      error => {
        console.log(<any>error)
      }
    );
  }

  onDelete(id: string){
    this.labelService.deleteDetail(id).subscribe(
      response => {
        console.log(response);
        this.getDetails();
      },
      error => {
        console.log(<any>error);
      }
    );
  }


  onSearch(id: string){
    this.labelService.getLabelsDetail(id).subscribe(
      response => {
        console.log(response['data']);
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  onAdd(){
    this.router.navigate(['admin/detail/']);
  }
}
