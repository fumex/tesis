import { ActivatedRoute, Router } from '@angular/router';
import { LabelDetailModel } from '../models/label_detail.model';

import { LabelService } from '../services/label.service';
import { Component, OnInit } from '@angular/core';
import { LabelModel } from '../models/label.model';


@Component({
  selector: 'detail-add',
  templateUrl: '../views/detail-add.html',
  providers: [LabelService]
})
export class DetailAddComponent implements OnInit{
  public title: string;
  public button:string;
  public detail: LabelDetailModel;
  public labels: any[];
  constructor(
    private detailService: LabelService,
    private route: ActivatedRoute,
    private router: Router
  ){
    this.title = 'Tratamiento';
    this.button = 'Registrar';
    this.detail = new LabelDetailModel('', '', '');
  }
  ngOnInit(){
    this.getLabel();
  }

  getLabel(){
    this.detailService.getLabels().subscribe(
      response =>{
        this.labels = response['data'];
      },
      error => {
        console.log(<any>error)
      }
    );
  }

  onSubmit(){
    this.detailService.addLabelDetail(this.detail).subscribe(
      response =>{
        console.log(response);
        this.router.navigate(['admin/detail/list']);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onList(){
    this.router.navigate(['admin/detail/list']);
  }
}
