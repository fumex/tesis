import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LabelModel } from '../models/label.model';
import { LabelService } from '../services/label.service';

@Component({
  selector: 'label-views',
  templateUrl: '../views/label-view.html'
})
export class LabelViewComponent implements OnInit{
  public label: LabelModel;
  public labels: any[];
  public name: string;
  public symbol: string;
  public description: string;
  constructor(
    private labelService: LabelService,
    private route:ActivatedRoute,
    private router:Router
  ){
    this.label = new LabelModel('','','','');
  }
  ngOnInit(){
    this.getLabel();
  }

  getLabel(){
    this.route.params.forEach((params: Params) => {
      let id = params['id'];
      this.labelService.getLabel(id).subscribe(
        response => {
          this.label = response['data'];
          this.asigLabel(this.label);
          this.getDetail(id);
        },
        error => {
          console.log(<any> error);
        }
      );
    });
  }

  getDetail(id: string){
    this.labelService.getLabelsDetail(id).subscribe(
      response => {
        this.labels = response['data'];
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  asigLabel(label:LabelModel){
    this.name = label.name;
    this.symbol = label.symbol;
    this.description = label.description;
  }
  onList(){
    this.router.navigate(['admin/label/list']);
  }
  onPredict(){
    this.router.navigate(['admin/prediction']);
  }
}
