import { ActivatedRoute, Router, Params } from '@angular/router';

import { LabelModel } from './../models/label.model';
import { Component, OnInit } from '@angular/core';
import { LabelService } from '../services/label.service';

@Component({
  selector:'label-edit',
  templateUrl: '../views/label-add.html',
  providers:[LabelService]
})
export class LabelEditComponent implements OnInit{
  public title: string;
  public label: LabelModel;

  constructor(
    private labelService:LabelService,
    private route:ActivatedRoute,
    private router:Router
  ){
    this.title = 'Editar Nutriente';
    this.label= new LabelModel('', '', '', '');
  }
  ngOnInit(){
    this.getLabel();
  }

  getLabel(){
    this.route.params.forEach((params:Params)=>{
      let id = params['id'];
      console.log(id);
      this.labelService.getLabel(id).subscribe(
        response => {
          this.label = response['data'];
          console.log(this.label);
        },
        error => {
          console.log(<any>error);
        }
      );
    });
  }
  onSubmit(){
    this.route.params.forEach((params: Params) => {
      let id= params['id'];
      this.labelService.updateLabel(id, this.label).subscribe(
        result=>{
          console.log(result);
          this.onList();
        },
        error=>{
          console.log(<any>error);
        }

      );
    });
  }
  onList(){
    this.router.navigate(['admin/label/list']);
  }
}
