import { LabelModel } from './../models/label.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LabelService } from '../services/label.service';

@Component({
  selector:'label-list',
  templateUrl:'../views/label-list.html',
  providers:[LabelService]
})
export class LabelListComponent implements OnInit{
  public labels:any[];
  constructor(
    private labelService: LabelService,
    private route: ActivatedRoute,
    private router:Router
  ){

  }

  ngOnInit(){
    this.getLabels();
  }

  getLabels(){
    this.labelService.getLabels().subscribe(
      response => {
          this.labels = response['data'];
      },
      error => {
        console.log(<any>error);
      }
    )
  }
  onDeleteLabel(id){
    this.labelService.deleteLabels(id).subscribe(
      response => {
        console.log(response);
        this.getLabels();
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onEdit(id: string){
    this.router.navigate(['admin/label/edit/', id]);
  }
  onAdd(){
    this.router.navigate(['admin/label/']);
  }

  onView(id:string){
    this.router.navigate(['admin/label/views/', id]);
  }

}
