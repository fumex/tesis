import { Router } from '@angular/router';
import { LabelModel } from './../models/label.model';
import { LabelService } from './../services/label.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'label-add',
  templateUrl: '../views/label-add.html',
  providers: [LabelService]
})
export class LabelAddComponent implements OnInit{

  public title: String;
  public label: LabelModel;

  constructor(
    private labelService: LabelService,
    private router:Router
  ){
    this.title = 'Registar Nutriente';
    this.label= new LabelModel('','','','');
  }
  ngOnInit(){

  }

  onSubmit(){
    this.labelService.addLabel(this.label).subscribe(
      response => {
        console.log(response);
        this.clearLabel();
        this.onList();
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onList(){
    this.router.navigate(['admin/label/list']);
  }
  clearLabel(){
    this.label= new LabelModel('','','','');
  }
}

