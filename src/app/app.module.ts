import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing/app-routing.module'
import { LabelModule } from './label/label.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PredictionsModule } from './prediction/prediction.module';




@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    HttpClientModule,

    FormsModule,
    AppRoutingModule,
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    LabelModule,
    PredictionsModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
