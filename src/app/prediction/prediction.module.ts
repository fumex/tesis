import { PredictionService } from './services/prediction.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PredictionComponent } from './components/prediction.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [
    PredictionComponent
  ],
  providers: [
    PredictionService
  ]
})
export class PredictionsModule{}
