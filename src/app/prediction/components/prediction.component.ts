import { PredictionService } from './../services/prediction.service';
import { Component, OnInit } from '@angular/core';
import { LabelService } from '../../label/services/label.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector:'prediction-add',
  templateUrl:'../views/prediction.html',
  providers:[PredictionService]
})
export class PredictionComponent implements OnInit{

  //LOADER
  public loader:boolean; 
  //Subir archivos
  public imagenUrl: string;
  public noImagen: string;
  public filear;
  public filesToUpload: File[];
  public fileToUpload: File = null;

  //Muestra probabilidades  
  public elemento1;
  public elemento2;
  public elemento3;
 
  public label:any[];
  public id_1:string;
  public id_2:string;
  public id_3:string;


  public visible:Boolean;
  public habilitado:Boolean;
  //--- Graphic
  public pieChartLabels:string[] = ["Asintomática", "Calcio", "Potasio", "Magnesio", "Nitrógeno","Fósforo","Azufre"];
  public pieChartData1:number[] = [14,20,15,6,20,10,15];
  public pieChartData2:number[] = [16,15,11,8,16,27,7];
  public pieChartData3:number[] = [14,6,17,31,6,6,13];
  public pieChartOptions:any={
    responsive:true,
    legend: {
      position: 'left',
    }
  }

  public pieChartType:string = 'pie';
  public color1:any = [{'backgroundColor': ['#FF8A47', '#FC6170','#77A1A6', '#26BFBF','#FFD747','#DC143C','#2F4F4F']}];
  public color2:any = [{'backgroundColor': ['#FF8A47', '#FC6170','#77A1A6', '#26BFBF','#FFD747','#DC143C','#2F4F4F']}];
  public color3:any = [{'backgroundColor': ['#FF8A47', '#FC6170','#77A1A6', '#26BFBF','#FFD747','#DC143C','#2F4F4F']}];
  //---end Graphic

  //----------Graphic BAR--------------------------
  public barChartOptions:any={
    scaleShowVerticalLines:false,
    responsive:true,
  };
  
  public barChartLegend:boolean = true;
  public barChartType:string = 'bar';
  public barChartData:any[] = [
    { data: [14,20,15,6,20,10,15], label: 'Inception-v3' },
    { data: [16,15,11,8,16,27,7], label: 'DenseNet' },
    { data: [14,6,17,31,6,6,13], label: 'MobileNet' }
  ];

  //----------end Graphic BAR---------------------- 

  //variables de tiempo
  public tiempoModel:number[];
  constructor(
    private predictionService: PredictionService,
    private labelService:LabelService,
    private route: ActivatedRoute,
    private router:Router
  ){
    this.noImagen = '../../../assets/img/no_photo.jpg';
    this.visible = true;
    this.habilitado=true;
    this.id_1='';
    this.id_2='';
    this.id_3='';
    this.loader=true;
  }
  ngOnInit(){
    this.imagenUrl = this.noImagen;
    this.listLabel();
  }
//Subir imagen al servidor
  viewImage(file: FileList, fileInput: any) {
    this.filear = document.getElementById('file');
    const allowedExtensions = /(.jpg|.jpeg|.png)$/i;
    if (!this.filear.value) {
      console.log('nada se encontro archivo');
      this.filesToUpload = null;
      this.imagenUrl = this.noImagen;
    } else {
      console.log('entro');
      this.filesToUpload = fileInput.target.files;
      this.fileToUpload = file.item(0);
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.imagenUrl = event.target.result;
      }
      reader.readAsDataURL(this.fileToUpload);
      console.log('listo y mostrados');
      this.habilitado=false;
    }
  }

  onSubmit(){
    this.predictionService.postFile(this.fileToUpload).subscribe(
      response => {
        this.loader=false;
        this.onPrediction();
        console.log('cargando...');
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onPrediction(){
    this.predictionService.getPrediction().subscribe(
      response => {
        this.pieChartData1 = response['data'][0]; //Inception
        this.pieChartData2 = response['data'][1]; //DenseNet
        this.pieChartData3 = response['data'][2]; //MobileNet
        let tiempo = response['data'][3] //tiempo de ejecucion
        let incept = this.time_second(tiempo[0]);
        let dense = this.time_second(tiempo[1]);
        let mobile = this.time_second(tiempo[2]);

        this.loader=true;
        console.log(response);

        this.barChartData =[
          { data: response['data'][0],
             label: `Inception-v3: ( tiempo = ${incept[0]}s ${incept[1]} ms )` },
          { data:response['data'][1],
             label: `DenseNet:( tiempo = ${dense[0]}s ${dense[1]}ms )` },
          { data: response['data'][2],
             label: `MobilNet: ( tiempo = ${mobile[0]}s ${mobile[0]}ms )` }
        ];
        console.log('Termino Graficar')
        this.elemento1 = this.normalizarArreglo(response['data'][0]);
        this.elemento2 = this.normalizarArreglo(response['data'][1]);
        this.elemento3 = this.normalizarArreglo(response['data'][2]);
         //console.log(response['data'][0]);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  time_second(n){
    let numero= n.toFixed(2);
    let tmp = (numero+"").split('.');
    return tmp;
  }


  listLabel(){
    this.labelService.getLabels().subscribe(
        response => {
          this.label=response['data'];
          console.log(this.label);
        },
        error => {
          console.log(<any>error);
        }
    )
  }

  buscarSimbolo(symbol, data){
    let _id;
    for(let i=0; i<data.length; i++){
      if( symbol == data[i]['symbol']){
        _id = data[i]['id'];
        console.log('------Buscar Simbolo-----')
        console.log(_id)
        break;
      }
    }
    return _id;
  }

  // events on slice click
  public chartClicked(e:any):void {
    console.log(e);
  }
 
 // event on pie chart slice hover
  public chartHovered(e:any):void {
    console.log(e);
  }

  normalizarArreglo(datos){
    let mayor = 0;
    let i;
    for(i = 0; i<datos.length; i++){
      if( datos[i]>mayor){
        mayor = datos[i]; 
      }
    }
    return this.resultado(datos, mayor)
  }
  resultado(datos, mayor){
    let etiqueta = datos.indexOf(mayor);
    let simbolo;
    let elemento;
    let exactitud;
    let _id;
    let valores=[];
    this.visible = false;
    switch (etiqueta){
      case 0:
        simbolo = 'A';
        elemento = 'Asintomática';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;
      case 1:
        simbolo='Ca';
        elemento = 'Calcio';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;

      case 2:
        console.log('K');
        elemento = 'Potasio';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;
      case 3:
        simbolo = 'Mg';
        elemento = 'Magnesio';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;

      case 4:
        simbolo = 'N';
        elemento = 'Nitrógeno';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;
      case 5:
        simbolo='P';
        elemento = 'Fósforo';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;

      case 6:
        simbolo='S';
        elemento = 'Azufre';
        exactitud = mayor;
        _id =  this.buscarSimbolo(simbolo,this.label);
        valores.push(elemento,exactitud,_id);
        console.log(valores);
        return valores;
    }
  }

  onView(id){
    this.router.navigate(['admin/label/views/',id]);
  }
}
