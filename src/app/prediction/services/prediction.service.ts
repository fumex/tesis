import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
@Injectable()
export class PredictionService{
  constructor(
    private http: HttpClient
  ){}

  postFile(fileToUpload: File){
    let formData: FormData =  new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    let headers =  new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${environment.api_url}/upload`, formData, {headers: headers});
  }

  getPrediction():Observable<any>{
    return this.http.get(`${environment.api_url}/prediccion`);
  }

}
